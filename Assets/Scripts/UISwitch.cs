﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(CanvasGroup))]
public class UISwitch : MonoBehaviour {
    [HideInInspector]
    public List<GameObject> objects;
    CanvasGroup canvasGroup {  get { return GetComponent<CanvasGroup>(); } }

    public void Switch() {
        if (canvasGroup.blocksRaycasts) {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
        } else {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
        }
    }

    public void Switch (bool state) {
        if (state) {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
        } else {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
        }

        for (int i = 0; i < objects.Count; i++) {
            objects[i].SetActive(state);
        }
    }
}
