﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum CheckerActionType { Teleport, Events, StageComplete }

public class ZoneDetection : MonoBehaviour {
    [Header("Teleportation Settings")]
    public Transform endPoint;
    [Header("Zone Detection Type")]
    public CheckerActionType actionType;
    [Header("Events Settings")]
    public UnityEvent events;
    public bool once = true;
    bool active = true;

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            SelectAction();
        }
    }

    void SelectAction() {
        if (!active)
            return;

        switch(actionType) {
            case CheckerActionType.Teleport:
                PlayerCtrl.ctrl.Teleport(endPoint.position);
                break;

            case CheckerActionType.StageComplete:
                GameManager.ctrl.BackToMenu();
                break;

            case CheckerActionType.Events:
                events.Invoke();
                break;
        }

        if (once) 
            active = false;
    }
}
