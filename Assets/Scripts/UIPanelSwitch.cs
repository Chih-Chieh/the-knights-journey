﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelSwitch : MonoBehaviour {
    public RectTransform BGPanelRT;
    public int Count { // Number of scroll panels
        get { return BGPanelRT.childCount - 1; } // EXCLUDES the first StageTMP 
    }

    Vector2 CellCount {
        // Scroll panel dimensions are screen width * scroll panels count, and screen height
        get { return new Vector2(Screen.width * Count, Screen.height);  }
    }
    int index;
    Vector2 pos;
    public bool loopable = true;

    // Use this for initialization
    public void Init() {
        BGPanelRT.sizeDelta = CellCount; // Setting the scroll panel dimensions
	}

    public void Left() {
        index--;
        index = (index < 0) ? (loopable ? Count - 1 : 0) : index;
        pos.x = -Screen.width * index;
        BGPanelRT.anchoredPosition = pos;
    }

    public void Right() {
        index++;
        index = (index >= Count) ? (loopable ? 0 : Count - 1) : index;
        pos.x = -Screen.width * index;
        BGPanelRT.anchoredPosition = pos;
    }
}
