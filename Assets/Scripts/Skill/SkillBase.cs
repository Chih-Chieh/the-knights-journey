﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class SkillBase : MonoBehaviour {
    public int skillID;
    public SkillType skillType;
    public ActionType actionType;
    public UnityEvent attackEvents, buffEvents, ccEvents;
    public int hitCount = 1;
    protected int hitCountMax;

    [Header("技能銷毀距離")]
    Vector3 startPos;
    public float recycleDist = -1;
    [Header("技能銷毀時間")]
    float recycleTimer;
    public float recycleSec;

    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        startPos = transform.position;
        hitCountMax = hitCount;
    }

    public virtual void Event() {
        switch (skillType) {
            case SkillType.Attack:
                attackEvents.Invoke();
                break;
            case SkillType.Buff:
                buffEvents.Invoke();
                break;
            case SkillType.CC:
                ccEvents.Invoke();
                break;
        }
    }

    public abstract void Action();

    public virtual void Recycle() {
        gameObject.SetActive(false);
        GameManager.ctrl.recycleSystem.RecycleObj(this);
    }

    public virtual bool RecycleByDistance() {
        if (recycleDist == -1)
            return false;
        if (Vector3.Distance(transform.position, startPos) >= recycleDist) {
            gameObject.SetActive(false);
            GameManager.ctrl.recycleSystem.RecycleObj(this);
            return true;
        }
        return false;
    }

    public virtual bool RecycleByTime() {
        recycleTimer += Time.deltaTime;
        if (recycleTimer >= recycleSec) {
            gameObject.SetActive(false);
            GameManager.ctrl.recycleSystem.RecycleObj(this);
            return true;
        }
        return false;
    }

    public void Active() {
        recycleTimer = 0;
        startPos = transform.position;
        hitCount = hitCountMax;
        gameObject.SetActive(true);
    }
}