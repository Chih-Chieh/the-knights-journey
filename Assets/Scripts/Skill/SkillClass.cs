﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillClass {

}

public class SpeedUpBuff : Buff {
    public const float length = 15f;
    public const float speedModifier = 1.5f;
    public float speedMod {
        get { return speedModifier; }
    }

    public SpeedUpBuff() : base(length) {
        GameManager.ctrl.buffSystem.AddBuff(this);
    }
}
