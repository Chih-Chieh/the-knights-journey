﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundExplosion : SkillBase {
    [Range(1, 30)]
    public float triggerRange = 5;
    public float attackDamage = 100;

    public override void Action() {
        if (hitCount <= 0)
            return;
        for (int i = 0; i < GameManager.ctrl.targetSystem.Count(); i++) {
            MonsterCtrl target = GameManager.ctrl.targetSystem.Peek(i);
            if (transform.CircleCheck(target.transform, triggerRange)) {
                target.Hurt(attackDamage);
                //Event();
            }
        }

        RecycleByTime();
        // Invoke("Recycle", 1);
    }
}
