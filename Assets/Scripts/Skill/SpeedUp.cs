﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : SkillBase {

	public override void Action() {
        RecycleByTime();
        if (hitCount <= 0)
            return;

        new SpeedUpBuff(); //視覺特效表演同時，啟動BUFF

        hitCount--;
    }
}
