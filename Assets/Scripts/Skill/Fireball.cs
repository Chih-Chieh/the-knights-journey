﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : SkillBase {
    [Range(1, 30)]
    public float moveSpeed = 1;
    [Range(1, 30)]
    public float triggerRange = 1;
    public float attackDamage = 1;
    public float attackTimeRange = 1;
    Timer m_attackCD;
    Timer attackCD {
        get {
            if (m_attackCD == null) m_attackCD = new Timer(attackTimeRange);
            return m_attackCD;
        }
    }
    public GameObject hitEffect, hitSkillObj;

    public override void Event() {
        //base.Event();
        //Instantiate(hitEffect, transform.position, transform.rotation);
        GameManager.ctrl.PlaySFX(1);
        hitCount--;
        if (hitCount <= 0) {
            InstantiateHitSkill();
            Recycle();
        }
    }

    public override void Action() {
        // Recycles based on time and recycles based on distance travelled
        if (RecycleByDistance() || RecycleByTime()) {
            Instantiate(hitEffect, transform.position, transform.rotation);
        }

        if (actionType == ActionType.Fly)
                transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        if (attackCD.isDone) {
            for (int i = 0; i < GameManager.ctrl.targetSystem.Count(); i++) {
                MonsterCtrl target = GameManager.ctrl.targetSystem.Peek(i);
                if (transform.CircleCheck(target.transform, triggerRange, 180f)) { //180 so only the fronthalf of the fireball hits
                    target.Hurt(attackDamage);
                    Event();
                    attackCD.Start();
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Wall")) {
            //Instantiate(hitEffect, transform.position, transform.rotation); // General explode effect
            //Instantiate(hitSkillObj, transform.position, transform.rotation);
            InstantiateHitSkill();
            Recycle();
        }
    }

    void InstantiateHitSkill() {
        SkillBase skill = GameManager.ctrl.recycleSystem.GetSkill(100); // ensures we get the groundExplosion from recycle List
        if (skill != null) {
            skill.transform.position = transform.position;
            skill.transform.rotation = transform.rotation;
            skill.Active(); // Resets all the values first, then activate
        } else
            Instantiate(hitSkillObj, transform.position, transform.rotation);
    }

}
