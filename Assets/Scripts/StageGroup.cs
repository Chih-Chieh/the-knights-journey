﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct SceneInfo {
    public List<StageInfo> stageInfos;
}

[System.Serializable]
public struct StageInfo {
    public string stageName;
    public int cost;
    public Sprite icon;
    public List<StageMission> missions;
}

[System.Serializable]
public struct StageMission {
    public string description;
    public int targetID;
    public int countRequired;
    int count;
    public bool isComplete {
        get { return count >= countRequired; }
    }


    public void CountUpdate(int num = 1) {
        count += num;
        if (count >= countRequired)
            count = countRequired;
    }

    public string InfoReport() {
        return "►" + description.ToString() + " (" + count.ToString() + "/" + countRequired.ToString() + ")"
            + (isComplete ? " (Done)" : "");
    }

}

public class StageGroup : MonoBehaviour {
    public List<StageInfo> stageInfos;
    [System.Serializable]
    public struct Btns {
        public Button button;
        public Text textLabel;
        public Image icon;
    }
    public Btns[] btns = new Btns[5];
    //public Text[] textLabs = new Text[5];

    public void SetStageData (List<StageInfo> stageInfos) {
        gameObject.SetActive(true);
        this.stageInfos = stageInfos;

        for (int i = 0; i < btns.Length; i++) {
            string stageName = stageInfos[i].stageName;
            int cost = stageInfos[i].cost;
            List<StageMission> missions = stageInfos[i].missions;
            if (stageInfos[i].icon)
                btns[i].icon.sprite = stageInfos[i].icon; // Replace stage icon (if avaliable)
            btns[i].textLabel.text = stageName;
            btns[i].button.onClick.AddListener(delegate { GoToStage(stageName, cost, missions); });
        }
    }

    public void GoToStage(string stageName, int cost, List<StageMission> missions) {
        if (GameManager.ctrl.infoSystem.SetEnergy(-cost)) {
            GameManager.ctrl.infoSystem.uiCtrl.Switch(false); //switches off UI when changing stage
            GameManager.ctrl.GetSkillInfo(); // Read the chosen skills info
            GameManager.ctrl.GetMissionData(missions);
            GameData.ctrl.Save();
            SceneManager.LoadScene("Stage" + stageName);
        }
    }
}
