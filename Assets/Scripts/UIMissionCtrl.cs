﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMissionCtrl : MonoBehaviour {

    public Text missionText;
    public StageMission mission;

    public void SetMissionData(StageMission mission) {
        this.mission = mission;
        gameObject.SetActive(true);
        missionText.text = this.mission.InfoReport();
    }

    // Check if the mission target ID matches the killed monster ID
    public void CheckMissionData(int ID) {
        if (mission.targetID == ID) {
            mission.CountUpdate();
            missionText.text = mission.InfoReport();//任務資訊更新
            GameManager.ctrl.hudSystem.CheckMission();//檢查是否任務全部完成
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
