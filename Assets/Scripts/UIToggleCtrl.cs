﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToggleCtrl : MonoBehaviour {
    public UISwitch obj1, obj2, obj3;
    int index = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Changes the camera position to "hide" the character model
    public void CameraSwitch () {
        Camera.main.transform.position = Vector3.forward * 10 * index;
    }

    public void ToggleSwitch (Toggle toggle) {
        //print(toggle.name + toggle.isOn);

        switch(toggle.name) {
            case "CharacterToggle":
                obj1.Switch(toggle.isOn);
                if (toggle.isOn) {
                    index = 0;
                    CameraSwitch();
                }
                break;

            case "MissionToggle":
                obj2.Switch(toggle.isOn);
                if (toggle.isOn) {
                    index = 1;
                    CameraSwitch();
                }
                break;

            case "EnhanceToggle":
                obj3.Switch(toggle.isOn);
                if (toggle.isOn) {
                    index = 2;
                    CameraSwitch();
                }
                break;
        }
    }
}
