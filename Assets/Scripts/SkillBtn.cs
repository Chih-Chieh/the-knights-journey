﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillBtn : MonoBehaviour {
    public int skillID;
    public int cost;
    public float cd, cdTimeNow;
    public Image cdImg;
    public Text cdText;

	public void SetData(float cd, int skillID) {
        this.skillID = skillID;
        this.cd = cd;
        cdImg.fillAmount = 0;
        cdText.text = string.Empty;
    }
	
	public void Active() {
        if (cdTimeNow > 0)
            return;
        cdTimeNow = cd;
        GameManager.ctrl.SkillCaller(skillID);
    }

    void Update() {
        if (cdTimeNow > 0) {
            cdTimeNow -= Time.deltaTime;
            cdImg.fillAmount = cdTimeNow / cd;
            cdText.text = cdTimeNow.ToString("F1");
        } else {
            cdText.text = string.Empty;
        }
    }
}
