﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRewardPanelCtrl : MonoBehaviour {
    public UISlotCtrl slotTMP;
    public Transform slotContent;
    public List<UISlotCtrl> slotCtrls = new List<UISlotCtrl>();
    public Text moneyText, expText;
    public UISwitch uiSwitch {
        get { return GetComponent<UISwitch>(); }
    }
    int index = 0; // Used to track whether new slot is required

	// Use this for initialization
	void Start () {
		
	}
	
	public void SetRewardData (int money, int exp) {
        uiSwitch.Switch(true);
        moneyText.text = money.ToString();
        expText.text = exp.ToString();
    }

    // Index is used to compare with slotCtrls count to check
    // whether new slot is needed (if greater). Otherwise just
    // reactivate old slots (object pooling)
    public void SetDropItems(ItemData itemData, int count) {
        if (index >= slotCtrls.Count) {
            UISlotCtrl slotCtrlTMP = Instantiate(slotTMP, slotContent);
            slotCtrls.Add(slotCtrlTMP);
            slotCtrlTMP.SetData(itemData, count);
            slotCtrlTMP.gameObject.SetActive(true);
        } else {
            slotCtrls[index].SetData(itemData, count);
            slotCtrls[index].gameObject.SetActive(true);
        }

        index++;
    }

    // Upon pressing confirm button, all slots data are resetted
    public void ResetRewardData() {
        index = 0;
        for (int i = 0; i < slotCtrls.Count; i++) {
            slotCtrls[i].gameObject.SetActive(false);
        }
    }
}
