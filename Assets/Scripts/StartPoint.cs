﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoint : MonoBehaviour {

    public static StartPoint ctrl;

    private void Awake() {
        ctrl = this;
    }

    public Vector3 GetPos(Transform target) {
        target.rotation = transform.rotation; // Makes player faces the direction this startPoint is facing
        return transform.position;
    }
}
