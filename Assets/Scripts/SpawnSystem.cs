﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem : MonoBehaviour {
    public List<MonsterCtrl> spawnObjs;
    List<Transform> points = new List<Transform>();
    public bool autoSpawn = true;
    public bool patrol = true;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < transform.childCount; i++) {
            points.Add(transform.GetChild(i));
        }

        if (autoSpawn)
            Invoke("Spawn", 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn() {
        foreach (Transform point in points) {
            int number = Random.Range(0, spawnObjs.Count);
            MonsterCtrl monster = GameManager.ctrl.recycleSystem.GetMonster(spawnObjs[number].monsterID);
            if (monster != null) {
                monster.transform.position = point.position + Vector3.up;
                monster.Active();
            } else {
                Instantiate(spawnObjs[number], point.position + Vector3.up, Quaternion.identity);
            }
        }
    }

    // Used for Events triggers
    public void Spawn(bool patrol) {
        foreach(Transform point in points) {
            int number = Random.Range(0, spawnObjs.Count);
            MonsterCtrl monster = GameManager.ctrl.recycleSystem.GetMonster(spawnObjs[number].monsterID);
            if (monster != null) {
                monster.transform.position = point.position + Vector3.up;
                monster.Active(patrol);
            } else {
                Instantiate(spawnObjs[number], point.position + Vector3.up, Quaternion.identity).patrol = patrol;
            }
        }
    }
}
