﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISlotCtrl : MonoBehaviour {
    public Image iconImg;
    public Text countText;

    // Use this for initialization
    void Start () {
		
	}

    public void SetData(ItemData itemData, int count) {
        iconImg.sprite = itemData.icon;
        countText.text = count.ToString();
    }
}
