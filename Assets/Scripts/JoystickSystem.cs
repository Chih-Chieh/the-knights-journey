﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class JoystickSystem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public static JoystickSystem ctrl;

    public RectTransform touchScreen {
        get {  return GetComponent<RectTransform>(); }
    }
    public RectTransform padBG {
        get { return touchScreen.GetChild(0).GetComponent<RectTransform>(); }
    }
    public RectTransform pad {
        get { return padBG.GetChild(0).GetComponent<RectTransform>(); }
    }
    Vector2 screenPos, pos;
    public CanvasGroup canvasGroup {
        get { return GetComponent<CanvasGroup>();  }
    }


    private void Awake() {
        ctrl = this;
    }

    /// <summary>
    /// Calculates the angles to rotate
    /// </summary>
    /// <returns>Rotating angle</returns>
    public float RotateAngle() {
        float angle = Vector2.Angle(Vector2.up, pos); // Calculates angle to rotate
        return pos.x > 0 ? angle : -angle; // Determines moving left or right
    }

    public bool IsUsingStick() {
        return pos != Vector2.zero;
    }

    public Vector2 GetStickPos() {
        return pos;
    }

    public void OnPointerDown(PointerEventData eventData) {
        Switch(true); // Pad visible on touch
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(touchScreen, eventData.position, eventData.pressEventCamera, out screenPos)) {
            padBG.anchoredPosition = screenPos;
        }
    }
    public void OnPointerUp(PointerEventData eventData) {
        Switch(false); // Pad invisible off touch
        pad.anchoredPosition = Vector2.zero;
        pos = Vector2.zero;
    }

    public void OnBeginDrag(PointerEventData eventData) {
        // Controls the pad to start following cursor
        OnDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData) {
        // Pad continues to follow cursor, must not exceed the boundary
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(padBG, eventData.position, eventData.pressEventCamera, out pos)) {
            pos.x = pos.x / (padBG.sizeDelta.x / 2);
            pos.y = pos.y / (padBG.sizeDelta.y / 2);
            if (pos.magnitude > 1)
                pos = pos.normalized;

            pad.anchoredPosition = pos * (padBG.sizeDelta.y / 2) * 0.7f; // how much the pad can move
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        // Reset pad
        Switch(false);
        pad.anchoredPosition = Vector2.zero;
        pos = Vector2.zero;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Switch(bool b) {
        if (b) {
            canvasGroup.alpha = 1;
            //canvasGroup.blocksRaycasts = true;
        } else {
            canvasGroup.alpha = 0;
            //canvasGroup.blocksRaycasts = false;
        }
    }
}
