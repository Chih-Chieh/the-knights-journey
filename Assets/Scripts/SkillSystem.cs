﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SkillToggleData {
    public Image iconImg;
    public int skillID;
}

[System.Serializable]
public struct SkillInfo {
    public string skillName;
    public int skillID;
    public Sprite icon;
    public string costDescription;
    public float cooldown;
    public GameObject skillObj;
    [TextArea(5, 10)]
    public string description;
}

public class SkillSystem : MonoBehaviour {
    public static SkillSystem ctrl;
    public SkillDB skillDB;
    public Transform skillContent;
    public SkillData skillTMP;
    public List<SkillToggleData> skillToggleDatas;
    int selectSlotNum;
    public Sprite defaultIcon;

	// Use this for initialization
	void Awake() {
        ctrl = this;
	}

    public void Init() {
        for(int i = 0; i < skillDB.skillInfos.Count; i++) {
            Instantiate(skillTMP, skillContent).SetData(skillDB.skillInfos[i]);
        }

        if (GameData.ctrl.skillCount == 0)
            return; // prevents running the json for-loop if savedata is new
        // Updates the slots to have json items
        for (int i = 0; i < GameData.ctrl.skillToggleDatas.Count; i++) {
            int id = GameData.ctrl.skillToggleDatas[i].skillID;
            skillToggleDatas[i].skillID = id;
            skillToggleDatas[i].iconImg.sprite = id < 0 ? defaultIcon : skillDB.SearchSkill(id).icon;
        }
    }

    public void SetSkillData(SkillInfo skillInfo) {
        if (skillToggleDatas[selectSlotNum].skillID == skillInfo.skillID)
            return;
        // Removes duplicate skill selections
        for (int i = 0; i < skillToggleDatas.Count; i++) {
            if (skillInfo.skillID == skillToggleDatas[i].skillID) {
                skillToggleDatas[i].iconImg.sprite = defaultIcon;
                skillToggleDatas[i].skillID = -1;
                break;
            }
        }
        // Setting skill
        skillToggleDatas[selectSlotNum].iconImg.sprite = skillInfo.icon;
        skillToggleDatas[selectSlotNum].skillID = skillInfo.skillID;
        // Saving after setting
        GameManager.ctrl.gameData.SkillSave(skillToggleDatas);
    }

    public void ToggleSwitch(int slotNum) {
        switch (slotNum) {
            case 0:
                selectSlotNum = 0;
                break;

            case 1:
                selectSlotNum = 1;
                break;

            case 2:
                selectSlotNum = 2;
                break;
        }
    }
}
