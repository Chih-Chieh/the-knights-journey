﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSystem : MonoBehaviour {

    public static CameraSystem ctrl;
    Transform target;
    Vector3 targetPos;

    public int targetDistance;
    [Range(15f, 80f)]
    public float angX = 35;
    [Range(0f, 360f)]
    public float angY;

    private void Awake() {
        ctrl = this;
    }

    // Use this for initialization
    void Start () {
        // Places the camera inside an empty gameobject
        target = new GameObject("CameraTarget").transform;
        transform.SetParent(target);
	}
	
	// Update is called once per frame
	void Update () {
        CameraFollow();
	}

    public void SetTargetPos(Vector3 destination) {
        targetPos = destination;
    }

    void CameraFollow() {
        // The camera position should be angle * distance from target (which follows player)
        transform.position = target.position + AngleFromGround() * DistanceFromTarget();
        target.position = Vector3.Lerp(target.position, targetPos, Time.deltaTime * 5f);
    }

    Vector3 DistanceFromTarget() {
        transform.LookAt(target); // Sets the rotation of camera to face target
        return Vector3.back * targetDistance; // How far BACK from target
    }

    Quaternion AngleFromGround() {
        return Quaternion.Euler(angX, angY, 0);
    }
}
