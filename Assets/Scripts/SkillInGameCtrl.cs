﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SkillCtrlData {
    [HideInInspector]
    public string skillName;
    [HideInInspector]
    public int skillID;
    [HideInInspector]
    public Sprite icon;
    [HideInInspector]
    public int cost;
    [HideInInspector]
    public float cd;
    public Image iconImg;
    public Button skillBtn;
    public SkillBtn btnCtrl;

    public void UpdateUI() {
        if (skillID >= 0) 
            iconImg.sprite = icon;
        skillBtn.interactable = skillID < 0 ? false : true;
        btnCtrl.SetData(cd, skillID);
    }
}

public class SkillInGameCtrl : MonoBehaviour {
    public List<SkillCtrlData> skillCtrlDatas;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < skillCtrlDatas.Count; i++) {
            SkillInfo skillInfo = GameManager.ctrl.GetSkillData(i);
            skillCtrlDatas[i].skillID = skillInfo.skillID;

            if (skillInfo.skillID >= 0) {
                skillCtrlDatas[i].skillName = skillInfo.skillName;
                skillCtrlDatas[i].icon = skillInfo.icon;
                //skillCtrlDatas[i].cost = skillInfo.cost;
                skillCtrlDatas[i].cd = skillInfo.cooldown;
            }

            skillCtrlDatas[i].UpdateUI();//資料讀取完畢後更新
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
