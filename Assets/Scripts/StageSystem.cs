﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSystem : MonoBehaviour {
    public static StageSystem ctrl;

    public StageInfoDB stageInfoDB;
    public StageGroup stageTMP;

    void Awake() {
        ctrl = this;
    }

    // Use this for initialization
    public void Init() {
		for (int i = 0; i < stageInfoDB.sceneInfos.Count; i++) {
            Instantiate(stageTMP, transform).SetStageData(stageInfoDB.sceneInfos[i].stageInfos);
        }

        //FindObjectOfType<UIPanelSwitch>().Init();
        GetComponentInParent<UIPanelSwitch>().Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
