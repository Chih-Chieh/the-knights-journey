﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillDB", menuName = "DB/SkillDB", order = 3)]
public class SkillDB : ScriptableObject {
    public List<SkillInfo> skillInfos;

    public SkillInfo SearchSkill(int skillID) {
        SkillInfo skillInfoTmp = new SkillInfo();
        skillInfoTmp.skillID = -1; // Setting new will set skillID = 0, this is to prevent that
        for (int i = 0; i < skillInfos.Count; i++) {
            if (skillID == skillInfos[i].skillID) 
                return skillInfoTmp = skillInfos[i];
        }

        return skillInfoTmp;
    }
}
