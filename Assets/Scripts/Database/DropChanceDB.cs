﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DropChanceDB", menuName = "DB/DropChanceDB", order = 2)]
public class DropChanceDB : ScriptableObject {
	public List<DropChanceInfo> dropChanceInfos;
}
