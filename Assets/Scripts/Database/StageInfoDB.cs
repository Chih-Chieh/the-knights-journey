﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//資源清單創建修飾
[CreateAssetMenu(fileName = "StageDB", menuName = "DB/StageDB", order = 0)]
public class StageInfoDB : ScriptableObject {
    public List<SceneInfo> sceneInfos;
}