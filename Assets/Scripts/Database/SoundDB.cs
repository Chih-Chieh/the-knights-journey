﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundDB", menuName = "DB/SoundDB", order = 4)]
public class SoundDB : ScriptableObject {
    public List<AudioClip> audioClips;
}
