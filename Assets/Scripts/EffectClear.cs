﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectClear : MonoBehaviour {

    public ParticleSystem ParticleSystem {
        get {
            return GetComponent<ParticleSystem>();
        }
    }

    // Use this for initialization
    void Start () {
        Destroy(gameObject, ParticleSystem.main.duration);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
