﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager ctrl;
    public bool newGame;
    InfoSystem m_infoSystem;
    public InfoSystem infoSystem {
        get {
            if (m_infoSystem == null)
                m_infoSystem = InfoSystem.ctrl;
            return m_infoSystem;
        }
    }

    StageSystem m_stageSystem;
    public StageSystem stageSystem {
        get {
            if (m_stageSystem == null)
                m_stageSystem = StageSystem.ctrl;
            return m_stageSystem;
        }
    }

    TargetSystem m_targetSystem;
    public TargetSystem targetSystem {
        get {
            if (m_targetSystem == null)
                m_targetSystem = new TargetSystem();
            return m_targetSystem;
        }
    }

    HUDSystem m_HUDSystem;
    public HUDSystem hudSystem {
        get {
            if (m_HUDSystem == null)
                m_HUDSystem = HUDSystem.ctrl;
            return m_HUDSystem;
        }
    }

    int tmpMoney = 0, tmpExp = 0;

    BagSystem m_BagSystem;
    public BagSystem bagSystem {
        get {
            if (m_BagSystem == null)
                m_BagSystem = BagSystem.ctrl;
            return m_BagSystem;
        }
    }
    public ItemDB itemDB;
    // Dictionary for holding total amount of items earned during level
    public Dictionary<int, int> dropItemsTMP = new Dictionary<int, int>();

    SkillSystem m_SkillSystem;
    public SkillSystem skillSystem {
        get {
            if (m_SkillSystem == null)
                m_SkillSystem = SkillSystem.ctrl;
            return m_SkillSystem;
        }
    }
    List<int> skillIDs = new List<int>();
    public SkillDB skillDB;

    RecycleSystem m_recycleSystem;
    public RecycleSystem recycleSystem {
        get {
            if (m_recycleSystem == null) m_recycleSystem = new RecycleSystem();
            return m_recycleSystem;
        }
    }

    public SoundDB bgmDB, sfxDB;
    private AudioSystem m_audioSystem;
    public AudioSystem audioSystem {
        get {
            if (m_audioSystem == null) m_audioSystem = new AudioSystem();
            return m_audioSystem;
        }
    }

    public GameData gameData;

    private BuffSystem m_buffSystem;
    public BuffSystem buffSystem {
        get {
            if (m_buffSystem == null) m_buffSystem = new BuffSystem();
            return m_buffSystem;
        }
    }

    void Awake() {
        ctrl = this;
        DontDestroyOnLoad(gameObject);
    }

    // The GameManager's Start() will handle all system's start sequences
    void Start () {
        if (newGame) // DELETE save if newGame is true
            PlayerPrefs.DeleteAll();
        // Loads gamedata. MUST BE FIRST
        gameData = JsonUtility.FromJson<GameData>(PlayerPrefs.GetString("GameData"));//JSON存檔替換
        if (gameData == null)
            gameData = new GameData();//新建存檔資料物件

        targetSystem.Clear();
        infoSystem.Init(); //系統初始化
        stageSystem.Init();
        bagSystem.Init();
        skillSystem.Init();
    }
	
	// Update is called once per frame
	void Update () {
        buffSystem.UpdateBuff(Time.deltaTime);
    }

    public void SkillCaller(int skillID) {
        SkillBase skill = recycleSystem.GetSkill(skillID);
        if (skill != null) {
            skill.transform.position = PlayerCtrl.ctrl.transform.position;
            skill.transform.rotation = PlayerCtrl.ctrl.transform.rotation;
            skill.Active(); // Resets all the values first, then activate
        } else {
            Instantiate(skillDB.SearchSkill(skillID).skillObj,
                PlayerCtrl.ctrl.transform.position, PlayerCtrl.ctrl.transform.rotation);
        }
    }

    public void HUDUpdate (float hp, float hpMax) {
        hudSystem.HealthBarUpdate(hp, hpMax);
    }

    public void HUDMoneyUpdate (int money = 0) {
        tmpMoney += money;
        hudSystem.MoneyUpdate(tmpMoney);
    }

    public void DropItemUpdate(int itemID) {
        //print("Item id: " + itemID);
        int count = 1;

        // Adds to dictionary 
        if (dropItemsTMP.TryGetValue(itemID, out count)) {
            dropItemsTMP[itemID] = count + 1;
        } else dropItemsTMP.Add(itemID, 1);

        //print("Amount: " + dropItemsTMP[itemID]);
    }

    void UpdateBag() {
        // key is ID, value is item count
        foreach (KeyValuePair<int, int> dic in dropItemsTMP) {
            ItemData itemData = itemDB.SearchItem(dic.Key);
            infoSystem.rewardPanelCtrl.SetDropItems(itemData, dic.Value); // Shows items in rewardsPanel as STACKED
            for (int i = 0; i < dic.Value; i++) {
                bagSystem.PushItem(itemData);
            }
        }
        dropItemsTMP.Clear();
    }

    public void GetExpUpdate(int exp) {
        tmpExp += exp;
    }

    // Called when entering the stage
    public void GetSkillInfo() {
        skillIDs.Clear();
        for (int i = 0; i < skillSystem.skillToggleDatas.Count; i++) {
            skillIDs.Add(skillSystem.skillToggleDatas[i].skillID);
        }
    }

    public SkillInfo GetSkillData(int index) {
        SkillInfo skillInfoTmp = new SkillInfo();
        skillInfoTmp.skillID = -1;
        for (int i = 0; i < skillDB.skillInfos.Count; i++) {
            if (skillIDs[index] == skillDB.skillInfos[i].skillID) 
                return skillInfoTmp = skillDB.skillInfos[i];
        }
        return skillInfoTmp;
    }

    /// <summary>
    /// 播放背景音樂
    /// </summary>
    /// <param name="bgmNumber"></param>
    public void PlayBGM(int bgmNumber, float volume, bool loop = true, float pitch = 1f) {
        audioSystem.PlayMusic(bgmDB.audioClips[bgmNumber], volume, loop, pitch);
    }
    /// <summary>
    /// 播放音效
    /// </summary>
    /// <param name="bgmNumber"></param>
    public void PlaySFX(int sfxNumber) {
        audioSystem.PlaySoundFX(sfxDB.audioClips[sfxNumber]);
    }

    //-------------關卡流程功能-------------
    List<StageMission> missions;
    public void GetMissionData(List<StageMission> missions) {
        this.missions = missions;
    }

    public StageTimer stageTimer;
    public void StageStart(float timeFinsh = 0) {
        stageTimer = new StageTimer(timeFinsh);
        foreach (StageMission mission in missions) {
            hudSystem.CreateMission(mission);
        }
    }

    public void MonsterInfoUpdate(MonsterCtrl monsterCtrl) {
        targetSystem.Remove(monsterCtrl);//目標系統更新
        recycleSystem.RecycleObj(monsterCtrl);//回收系統更新
        hudSystem.CheckMissionByMonster(monsterCtrl.monsterID);//任務系統更新
    }

    public void BackToMenu() {
        infoSystem.rewardPanelCtrl.SetRewardData(tmpMoney, tmpExp);
        infoSystem.SetBalance(tmpMoney);
        tmpMoney = 0;
        infoSystem.SetExp(tmpExp);
        tmpExp = 0;
        //-----回收-----
        for (int i = 0; i < targetSystem.Count(); i++) {
            targetSystem.Peek(i).Die();
            recycleSystem.RecycleObj(targetSystem.Peek(i));
        }
        targetSystem.Clear();// Clears all the monsters remain in targetSystem
        buffSystem.Clear();//BUFF系統清除
        //-----回收-----END
        infoSystem.uiCtrl.Switch(true);
        UpdateBag();
        SceneManager.LoadScene("Menu");
    }

    public void LoseToMenu() {
        tmpMoney = 0;
        tmpExp = 0;
        dropItemsTMP.Clear();//清除暫存物品紀錄(字典)
        //-----回收-----
        for (int i = 0; i < targetSystem.Count(); i++) {
            targetSystem.Peek(i).Die();
            recycleSystem.RecycleObj(targetSystem.Peek(i));
        }
        targetSystem.Clear();// Clears all the monsters remain in targetSystem
        buffSystem.Clear();//BUFF系統清除
        //-----回收-----END
        infoSystem.uiCtrl.Switch(true);
        SceneManager.LoadScene("Menu");
    }
}

/// <summary>
/// 關卡完成條件之一(計時器)
/// </summary>
public class StageTimer {
    float timeTotal;
    public float timeFinsh;
    public bool timeIsUp {
        get { return timeFinsh <= 0 ? false : timeTotal >= timeFinsh; }
    }
    float min, sec;

    public StageTimer(float timeFinsh) {
        this.timeFinsh = timeFinsh;
    }

    public string Tick(float time) {
        timeTotal += time;
        min = (int)timeTotal / 60; // (int) to remove the display issue
        sec = (int)timeTotal % 60;
        return min.ToString("00") + " : " + sec.ToString("00");
    }

}

public class BuffSystem {
    List<Buff> buffs;
    float buffSpeedMod = 1f;

    public BuffSystem() {
        buffs = new List<Buff>();
    }

    // Check buff duration
    public void UpdateBuff(float time) {
        for (int i = 0; i < buffs.Count; i++) {
            buffs[i].Tick(time);
            if (buffs[i].timeOver)
                RemoveBuff(buffs[i]);
        }
    }

    public void AddBuff(Buff buff) {
        buffs.Add(buff);
        switch (buff.GetType().Name) {
            case "SpeedUpBuff":
                buffSpeedMod *= ((SpeedUpBuff)buff).speedMod;
                break;
        }
    }

    public void RemoveBuff(Buff buff) {
        buffs.Remove(buff);
        switch (buff.GetType().Name) {
            case "SpeedUpBuff":
                buffSpeedMod /= ((SpeedUpBuff)buff).speedMod;
                break;
        }
    }

    public void Clear() {
        buffSpeedMod = 1f;
        buffs.Clear();
    }

    public float MoveSpeedUp(float moveSpeedBase) {
        return moveSpeedBase * buffSpeedMod;
    }
}

public class Buff {
    public float duration;
    public bool timeOver {
        get {
            return duration <= 0f;
        }
    }

    public Buff(float duration) {
        this.duration = duration;
    }

    public void Tick(float time) {
        duration -= time;
    }
}

public class RecycleSystem {
    List<SkillBase> skills;
    List<MonsterCtrl> monsters;

    public RecycleSystem() {
        skills = new List<SkillBase>();
        monsters = new List<MonsterCtrl>();
    }

    public int SkillCount() {
        return skills.Count;
    }

    public SkillBase GetSkill(int skillID) {
        SkillBase skillTMP = null;
        for (int i = 0; i < skills.Count; i++) {
            if (!skills[i].gameObject.activeSelf && skills[i].skillID == skillID) {
                skillTMP = skills[i];
                skills.RemoveAt(i);
                break;
            }
        }
        return skillTMP;
    }

    public MonsterCtrl GetMonster(int monsterID) {
        MonsterCtrl monsterTMP = null;
        for (int i = 0; i < monsters.Count; i++) {
            if (!monsters[i].gameObject.activeSelf && monsters[i].monsterID == monsterID) {
                monsterTMP = monsters[i];
                monsters.RemoveAt(i);
                break;
            }
        }

        return monsterTMP;
    }

    // T methods can accept any parameter types. Requires namespace: using System;
    public void RecycleObj<T>(T tmpObj) {
        object obj = tmpObj;
        Type type = obj.GetType(); // Retrieves the value of obj to check type later
        if (type.BaseType == typeof(SkillBase)) // .baseType retrieves the parent type
            skills.Add((SkillBase)obj);
        if (type == typeof(MonsterCtrl))
            monsters.Add((MonsterCtrl)obj);
    }
}

public class TargetSystem {
    List<MonsterCtrl> monsters;

    public TargetSystem() {
        monsters = new List<MonsterCtrl>();
    }

    public int Count() {
        return monsters.Count;
    }

    public MonsterCtrl Peek(int index) {
        return monsters[index];
    }

    public void Add(MonsterCtrl monster) {
        monsters.Add(monster);
        //Debug.Log(monsters.Count);
    }

    public void Remove(MonsterCtrl monster) {
        monsters.Remove(monster);
    }

    public void Clear() {
        //for (int i = 0; i < monsters.Count; i++) {
        //    monsters[i].Die();
        //    GameManager.ctrl.recycleSystem.RecycleObj(monsters[i]);
        //}
        monsters.Clear();
    }
}

public class AudioSystem {
    AudioSource m_BGM;
    public AudioSource audioSourceBGM {
        get {
            if (m_BGM == null) {
                m_BGM = new GameObject("BGM-AudioSource").AddComponent<AudioSource>();
                UnityEngine.Object.DontDestroyOnLoad(m_BGM);
            }
            return m_BGM;
        }
    }

    AudioSource m_SFX;
    public AudioSource audioSourceSFX {
        get {
            if (m_SFX == null) {
                m_SFX = new GameObject("SFX-AudioSource").AddComponent<AudioSource>();
                UnityEngine.Object.DontDestroyOnLoad(m_SFX);
            }
            return m_SFX;
        }
    }

    public void PlayMusic(AudioClip BGM, float volume = 1f, bool loop = true, float pitch = 1f) {
        audioSourceBGM.clip = BGM;
        audioSourceBGM.volume = volume;
        audioSourceBGM.loop = loop;
        audioSourceBGM.pitch = pitch;
        audioSourceBGM.Play();
    }

    public void PlaySoundFX(AudioClip SFX, bool loop = false, float pitch = 1f) {
        audioSourceSFX.loop = loop;
        audioSourceSFX.pitch = pitch;
        audioSourceSFX.PlayOneShot(SFX);
    }

}