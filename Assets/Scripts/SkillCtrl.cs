﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillType { Attack, CC, Buff }
public enum ActionType { Normal, Fly }

public class SkillCtrl : MonoBehaviour {
    public SkillBase skillBase;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        skillBase.Action();
	}

    public void SpawnObj (GameObject obj) {
        Instantiate(obj, transform.position, transform.rotation);
    }
}
