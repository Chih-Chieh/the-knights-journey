﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BagSystem : MonoBehaviour {
    public static BagSystem ctrl;
    public ItemDB itemDB;

    public int cellSize = 100;
    public int slotCount = 1;

    public RectTransform contentRT, slotBG;
    public GameObject slotTMP;

    public SlotData[] slotDatas;

    const int colCount = 4;
    float rowFloat { get { return (float)slotCount / colCount; } }
    int rowCount = 0;

    void Awake() {
        ctrl = this;
    }

	// Use this for initialization
	public void Init () {
        // if contains decimal then +1 (to compensate for round-down), decimal pt = new row
        rowCount = rowFloat % 1 == 0 ? (int) rowFloat : (int) (rowFloat + 1);
        slotDatas = new SlotData[slotCount];
        contentRT.sizeDelta = Vector2.up * cellSize * rowCount;
        for (int i = 0; i < slotCount; i++) {
            GameObject slot = Instantiate(slotTMP, slotBG);
            slot.SetActive(true);
            //-----背包格編碼-----
            slot.name = "Slot (" + i + ")";
            slot.AddComponent<BagSlotClick>().index = i;
            //-----背包格編碼-----END

            //創建背包格記憶體資料區塊，並設定控制對象UI的關聯
            slotDatas[i] = new SlotData(slot.transform.GetChild(0).GetComponent<Image>(), slot.GetComponentInChildren<Text>());
        }

        if (GameData.ctrl.slotCount == 0)
            return; // prevents running the json for-loop if savedata is new
        // Updates the slots to have json items
        ItemData itemDataTMP = new ItemData();
        for (int i = 0; i < GameManager.ctrl.gameData.slotDataList.Count; i++) {
            if (GameManager.ctrl.gameData.slotDataList[i].itemID < 0)
                continue; // skip if slot is empty
            itemDataTMP.itemName = GameManager.ctrl.gameData.slotDataList[i].itemName;
            itemDataTMP.itemID = GameManager.ctrl.gameData.slotDataList[i].itemID;
            itemDataTMP.maxCount = GameManager.ctrl.gameData.slotDataList[i].maxCount;
            itemDataTMP.icon = GameManager.ctrl.gameData.slotDataList[i].icon;
            itemDataTMP.itemBase = GameManager.ctrl.gameData.slotDataList[i].itemBase;
            slotDatas[i].SetData(itemDataTMP, GameManager.ctrl.gameData.slotDataList[i].count);
        }
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.KeypadPlus)) {
            PushItem(itemDB.itemDatas[Random.Range(0, 3)]);
        }

        if (Input.GetKey(KeyCode.KeypadMinus)) {
            for (int i = 0; i < slotDatas.Length; i++) {
                slotDatas[i].SetEmpty();
            }
        }
    }

    public void UseItem(int index) {
        if (slotDatas[index].itemBase == null)
            return;

        switch (slotDatas[index].itemBase.GetType().Name) {
            case "Consumables":
                if (((Consumables)slotDatas[index].itemBase).Use()) {
                    slotDatas[index].ConsumeData();
                    print("成功使用藥水");
                }
                break;
        }

        GameManager.ctrl.gameData.BagSave(slotDatas);
    }

    public void PushItem(ItemData itemData) {
        for (int i = 0; i < slotCount; i++) {
            // Skips to next slot if the slot's maxCount is reached
            if (!slotDatas[i].canPush)
                continue;

            // Only push if slot is EMPTY or SAME ID otherwise skip to next
            if (slotDatas[i].itemID == -1 || itemData.itemID == slotDatas[i].itemID) {
                slotDatas[i].SetData(itemData);
                break;
            } else
                continue;
        }

        GameData.ctrl.BagSave(slotDatas);
    }
}

[System.Serializable]
public class SlotData {
    public Image itemIcon;
    public Text countText;
    public string itemName;
    public int itemID;
    public int maxCount = 1, count;
    public Sprite icon;
    public Item itemBase;
    public bool canPush {
        get { return count < maxCount; }
    }

    public SlotData(Image itemIcon, Text countText) {
        this.itemIcon = itemIcon;
        this.countText = countText;
        SetEmpty();
    }

    public void SetEmpty() {
        itemIcon.color = Color.clear;
        countText.text = "";
        itemName = string.Empty;
        itemID = -1;
        maxCount = 1;
        count = 0;
        icon = null;
        itemBase = null;
    }

    public void SetData(ItemData itemData) {
        icon = itemData.icon;
        maxCount = itemData.maxCount;
        count++;
        itemName = itemData.itemName;
        itemID = itemData.itemID;
        itemIcon.color = Color.white;
        itemIcon.sprite = icon;
        itemBase = itemData.itemBase;
        countText.text = count.ToString();
    }

    public void SetData(ItemData itemData, int count) {
        icon = itemData.icon;
        maxCount = itemData.maxCount;
        this.count = count;
        itemName = itemData.itemName;
        itemID = itemData.itemID;
        itemIcon.color = Color.white;
        itemIcon.sprite = icon;
        itemBase = itemData.itemBase;
        countText.text = count.ToString();
    }

    public void ConsumeData(int consumption = 1) {
        if (count <= 0)
            return;
        count -= consumption;
        countText.text = count.ToString();
        if (count <= 0)
            SetEmpty();
    }
}
