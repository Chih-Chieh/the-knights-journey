﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AreaCheckExtension {

    /// <summary>
    /// 員行範圍檢測
    /// </summary>
    /// <param name="center">中心點</param>
    /// <param name="target">目標點</param>
    /// <param name="ranger">半徑</param>
    /// <param name="angle">角度</param>
    /// <returns>是否於範圍內</returns>
    public static bool CircleCheck (this Transform center, Transform target, float range, float angle = 360f) {
        Vector3 posOnPlane = Vector3.ProjectOnPlane(target.position, Vector3.up);
        float distance = Vector3.Distance(center.position, posOnPlane);
        float ang = Vector3.Angle(center.forward, posOnPlane - center.position);
        return distance <= range && ang <= angle/2f;
    }

    public static bool CircleCheck(this Transform center, Vector3 targetPos, float range, float angle = 360f) {
        Vector3 posOnPlane = Vector3.ProjectOnPlane(targetPos, Vector3.up);
        float distance = Vector3.Distance(center.position, posOnPlane);
        float ang = Vector3.Angle(center.forward, posOnPlane - center.position);
        return distance <= range && ang <= angle / 2f;
    }

    public static bool RectangleCheck (this Transform center, Transform target, float range, float width = 1f) {
        // Finds the target's world position (ignoring Y value)
        Vector3 posOnPlane = Vector3.ProjectOnPlane(target.position, Vector3.up);
        // Finds the distance between the target and self's vertical axis.
        float forwardDist = Vector3.Dot(posOnPlane - center.position, center.forward);
        // Finds the distance between the target and self's horizontal axis.
        float rightDist = Vector3.Dot(posOnPlane - center.position, center.right);

        // >= 0 and Abs are used for ensuring the target is INFRONT
        return forwardDist >= 0 && forwardDist <= range &&  Mathf.Abs(rightDist) <= width / 2f;
    }
}
