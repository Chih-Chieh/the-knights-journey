﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PositionExtension {
    /// <summary>
    /// Calculates position offset
    /// </summary>
    /// <param name="origPos">Starting position</param>
    /// <param name="offset">New offset</param>
    /// <returns>Returns Vector3</returns>
    public static Vector3 OffsetPos(this Vector3 origPos, Vector3 offset) {
        return origPos + offset;
    }

    /// <summary>
    ///  Calculates position offset
    /// </summary>
    /// <param name="orgPos">Starting position</param>
    /// <param name="x">X</param>
    /// <param name="y">Y</param>
    /// <param name="z">Z</param>
    /// <returns>Returns Vector3</returns>
    public static Vector3 OffsetPos(this Vector3 orgPos, float x = 0, float y = 0, float z = 0) {
        return orgPos + new Vector3(x, y, z);
    }

    /// <summary>
    /// Calculates CharacterController position offset
    /// </summary>
    /// <param name="orgPos">Starting position</param>
    /// <param name="charCtrl">Character controller</param>
    /// <returns>Returns Vector3</returns>
    public static Vector3 CharOffsetPos(this Vector3 orgPos, CharacterController charCtrl) {
        return orgPos + new Vector3(0, charCtrl.height / 2f, 0);
    }

    /// <summary>
    /// Calculates CharacterController position offset
    /// </summary>
    /// <param name="charCtrl">Character controller</param>
    /// <param name="orgPos">Starting position</param>
    /// <returns>Returns Vector3</returns>
    public static Vector3 CharOffsetPos(this CharacterController charCtrl, Vector3 orgPos) {
        return orgPos + new Vector3(0, charCtrl.height / 2f, 0);
    }

    /// <summary>
    /// Teleport an object based on Vector3 position
    /// </summary>
    /// <param name="target">The object to be moved</param>
    /// <param name="pos">Target Vector3 position</param>
    public static void MoveToPos(this Transform target, Vector3 pos) {
        target.position = pos;
    }
    /// <summary>
    /// Teleport an object based on target transform
    /// </summary>
    /// <param name="target">The object to be moved</param>
    /// <param name="to">Transform of the target</param>
    public static void MoveToPos(this Transform target, Transform to) {
        target.position = to.position;
    }
    /// <summary>
    /// Teleport an object based on float positions
    /// </summary>
    /// <param name="target">The object to be moved</param>
    /// <param name="x">X</param>
    /// <param name="y">Y</param>
    /// <param name="z">Z</param>
    public static void MoveToPos(this Transform target, float x = 0, float y = 0, float z = 0) {
        target.position = new Vector3(x, y, z);
    }
}
