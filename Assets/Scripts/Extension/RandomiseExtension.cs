﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomiseExtension {

	public static int RandomiseMoneyDrop(this int startNumber, int range) {
        int randomNumber = Random.Range(-range, range);
        return (startNumber + randomNumber) < 0 ? 0 : (startNumber + randomNumber);
    }
}
