﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemChecker : MonoBehaviour {
    public GameObject gameManager, uiCanvas;

	// Use this for initialization
	void Start () {        
        // Instaniates GM and uiInfo systems if they don't exist
		if (!FindObjectOfType<GameManager>()) {
            Instantiate(gameManager);
            Instantiate(uiCanvas);
        }
        GameManager.ctrl.PlayBGM(0, 0.2f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
