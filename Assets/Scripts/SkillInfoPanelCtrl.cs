﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillInfoPanelCtrl : MonoBehaviour {

    public Text skillNameText, costText, cdText, descriptionText;

	public void ShowSkillInfo (string skillName, string cost, string cd, string description) {
        skillNameText.text = skillName;
        costText.text = cost;
        cdText.text = cd;
        descriptionText.text = description;
    }
}
