﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

    public void ChangeSceneByNumber(int index) {
        SceneManager.LoadScene(index);
    }

    public void ChangeSceneByName(string name) {
        SceneManager.LoadScene(name);
    }
}
