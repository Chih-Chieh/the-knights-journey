﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionStatus { Normal, Attack, Teleport }

[RequireComponent(typeof(CharacterController))]
public class PlayerCtrl : MonoBehaviour {

    public static PlayerCtrl ctrl;
    public Animator animator {
        get { return GetComponentInChildren<Animator>(); }
    }
    public ActionStatus actionStatus = ActionStatus.Normal;
    public MonsterCtrl target;
    [Header("Basic stats")]
    public float moveSpeed = 5f;
    public float hp, hpMax = 100f;
    [Header("Attack stats")]
    public float attackRange = 3f;
    public float attackDamage = 10f;
    public float attackSpeed = 0.5f; // This is not the actual % increase!

    CharacterController charCtrl {
        get { return GetComponent<CharacterController>(); }
    }

    Timer m_attackCooldown;
    Timer attackCooldown {
        get {
            if (m_attackCooldown == null) m_attackCooldown = new Timer(1f / (1f + attackSpeed), true);
            return m_attackCooldown;
        }
    }

    TargetSystem targetSystem {
        get { return GameManager.ctrl.targetSystem; }
    }

    private void Awake() {
        ctrl = this;
    }

    // Use this for initialization
    void Start () {
        hp = hpMax; // Initialise hp
        transform.MoveToPos(charCtrl.CharOffsetPos(StartPoint.ctrl.GetPos(transform)));
        CameraSystem.ctrl.SetTargetPos(transform.position);
        GameManager.ctrl.HUDUpdate(hp, hpMax);
    }
	
	// Update is called once per frame
	void Update () {

        switch(actionStatus) {
            case ActionStatus.Normal:
                if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                    Move();
                break;

            case ActionStatus.Attack:
                Attack();
                break;
        }

        if (Input.GetKey(KeyCode.Space))
            actionStatus = ActionStatus.Attack;
        else SwitchNormalStatus();
    }

    void Move() {
        if (JoystickSystem.ctrl.IsUsingStick()) {
            CameraSystem.ctrl.SetTargetPos(transform.position); // Makes camera follow player
            charCtrl.SimpleMove(transform.forward * GameManager.ctrl.buffSystem.MoveSpeedUp(moveSpeed));
            transform.rotation = Quaternion.Euler(0, JoystickSystem.ctrl.RotateAngle() + CameraSystem.ctrl.angY, 0);
            animator.SetBool("Run", true);
        } else {
            animator.SetBool("Run", false);
        }
    }

    // Attack mechanic here
    void Attack() {
        if (attackCooldown.isDone) {
            animator.SetFloat("Attack_Speed", 1f + attackSpeed);
            animator.SetTrigger("Attack");
            attackCooldown.Start();
            for (int i = 0; i < targetSystem.Count(); i++) {
                target = targetSystem.Peek(i);
                if (transform.CircleCheck(target.transform, 3.5f, 160f)) {
                    //print("ATTACK!!!");
                    target.Hurt(attackDamage);
                }
            }
            //if (transform.RectangleCheck(target, 5f, 4f))
            //    print("ATTACK!!!"); 
        }
    }

    public void Hurt(float damage) {
        hp -= damage;
        GameManager.ctrl.HUDUpdate(hp, hpMax);
    }

    public void Teleport(Vector3 destination) {
        transform.MoveToPos(charCtrl.CharOffsetPos(destination));
        CameraSystem.ctrl.SetTargetPos(transform.position);
        actionStatus = ActionStatus.Teleport;

        Invoke("SwitchNormalStatus", 1f); // length of Teleport status before switching back to Normal
    }

    void SwitchNormalStatus() {
        actionStatus = ActionStatus.Normal;
    }
}
