﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEvent : MonoBehaviour {

    public GameObject effect;
    public Transform pos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnEffect() {
        Instantiate(effect, pos.position, pos.rotation);
    }
}
