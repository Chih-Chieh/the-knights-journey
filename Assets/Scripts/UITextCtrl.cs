﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextCtrl : MonoBehaviour {

    Text m_text;
    public Text text {
        get {
            if (m_text == null)
                m_text = GetComponent<Text>();
            return m_text;
        }
    }
    public bool state = true;
    public string switchTextOne, switchTextTwo;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TextChange() {
        state = !state;
        text.text = state ? switchTextOne : switchTextTwo;
    }
}
