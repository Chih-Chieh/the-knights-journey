﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillData : MonoBehaviour {

    public Image iconImage;
    public Button btn;
    public SkillInfo skillInfo;
    public SkillInfoPanelCtrl panelCtrl;

	// Use this for initialization
	void Start () {
        
	}

    public void ShowInfo() {
        panelCtrl.ShowSkillInfo(skillInfo.skillName, skillInfo.costDescription, skillInfo.cooldown.ToString(), skillInfo.description);
    }
	
    public void SetData(SkillInfo skillInfo) {
        gameObject.SetActive(true);
        this.skillInfo = skillInfo;
        iconImage.sprite = this.skillInfo.icon;

        btn.onClick.AddListener(delegate { SetSkillDataBtn(skillInfo); });
    }

    // Used for choosing the skill on menu
	public void SetSkillDataBtn(SkillInfo skillInfo) {
        GameManager.ctrl.skillSystem.SetSkillData(skillInfo);
    }
}
