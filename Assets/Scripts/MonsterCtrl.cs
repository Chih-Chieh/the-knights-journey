﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MonsterActionStatus { Idle, Move, Attack }

public class MonsterCtrl : MonoBehaviour {
    PlayerCtrl player {
        get { return PlayerCtrl.ctrl; }
    }
    CharacterController charCtrl {
        get { return GetComponent<CharacterController>(); }
    }
    public Animator animCtrl;
    public MonsterActionStatus MAS;

    [Header("Basic stats")]
    public int monsterID;
    public float moveSpeed = 1f;
    public float hp, hpMax = 100f;
    public int dropMoney, dropMoneyRange = 50, expGained;
    public DropChanceDB dropChanceDB;
    Dice dropDice;
    Dice dropCount;
    public int minDropCount = 1, maxDropCount = 1;

    [Header("Attack stats")]
    public float attackRange = 3f;
    public float attackDamage = 1f;
    public float attackTimeRange = 3f;
    [Header("Range stats")]
    public float spawnRange = 15f;
    public float searchRange = 10f;
    public float idleRange = 5f;

    public bool patrol = true;
    bool isDead;
    Vector3 spawnPos;
    int idleRandom;
    Timer idleTimer = new Timer(10, true);

    Timer m_attackCooldown;
    Timer attackCooldown
    {
        get
        {
            if (m_attackCooldown == null) m_attackCooldown = new Timer(attackTimeRange, true);
            return m_attackCooldown;
        }
    }

    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
        GameManager.ctrl.targetSystem.Add(this);
        spawnPos = transform.position;
        hp = hpMax;
        InvokeRepeating("IdleRandom", 0, 1.5f);
        SetDice();
    }

    // Finds the total drop chance from all items combined in dropChanceDB
    void SetDice() {
        float maxChance = 0;
        for (int i = 0; i < dropChanceDB.dropChanceInfos.Count; i++) {
            maxChance += dropChanceDB.dropChanceInfos[i].chance;
        }
        dropDice.Set(0, maxChance);
        dropCount.Set(minDropCount, maxDropCount + 1);
    }

    // IMPORTANT!
    // Everytime if the chance is not within the section, deduct that section chance from chance\
    // The number rolled from dice does not matter! but the range it falls into
    /// <summary>
    /// 掉落物品迴圈
    /// </summary>
    /// <param name="count">掉落物品數量</param>
    void DropItems(int count = 1) {
        int dropCount = count;
        //print("DROPS: " + dropCount);
        while (dropCount > 0) {//掉落次數執行
            float chance = dropDice.Report();//決定落點(在掉落表上的落點)
            //print("Drop chance: " + chance);
            for (int i = 0; i < dropChanceDB.dropChanceInfos.Count; i++) {
                if (chance < dropChanceDB.dropChanceInfos[i].chance) {//落在區間內(掉落)
                    GameManager.ctrl.DropItemUpdate(dropChanceDB.dropChanceInfos[i].itemID);
                    break;
                } else {//未在區間內，扣除該段機率繼續比對
                    chance -= dropChanceDB.dropChanceInfos[i].chance;
                }
            }
            dropCount--;//掉落次數完成減1
        }
    }

    void IdleRandom() {
        idleRandom = Random.Range(0, 3);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (isDead)
            return;

        ActionCheck();

        if (!transform.CircleCheck(spawnPos, spawnRange)) {
            MAS = MonsterActionStatus.Idle;
            transform.position = spawnPos;

        } else if (transform.CircleCheck(player.transform, attackRange)) {
            MAS = MonsterActionStatus.Attack;

        } else if (transform.CircleCheck(player.transform, searchRange)) {
            MAS = MonsterActionStatus.Move;

        } else {
            MAS = MonsterActionStatus.Idle;
        };
    }

    void ActionCheck() {
        switch(MAS) {
            case MonsterActionStatus.Attack:
                animCtrl.SetBool("Run", false);
                if (attackCooldown.isDone) {
                    attackCooldown.Start();
                    player.Hurt(attackDamage);
                    animCtrl.SetTrigger("Attack");
                    GameManager.ctrl.PlaySFX(3);
                }
                break;

            case MonsterActionStatus.Move:
                animCtrl.SetBool("Run", true);
                transform.LookAt(player.transform);
                charCtrl.SimpleMove(transform.forward * moveSpeed);
                break;

            // Idle behaviour (patrols around the idleRange)
            case MonsterActionStatus.Idle:
                if (idleRandom > 0 && patrol) {
                    animCtrl.SetBool("Run", true);
                    charCtrl.SimpleMove(transform.forward * moveSpeed * 0.5f); // Always moving while idle
                } else {
                    animCtrl.SetBool("Run", false);
                }
                if (transform.CircleCheck(spawnPos, idleRange)) {
                    if (idleTimer.isDone) {
                        idleTimer.Start();
                        transform.rotation = Quaternion.AngleAxis(Random.Range(60, 180), Vector3.up);
                    }
                } else {
                    if (!transform.CircleCheck(spawnPos, spawnRange, 90))
                        transform.rotation = Quaternion.AngleAxis(transform.rotation.y + Random.Range(30, 360), Vector3.up);
                }
                break;

            // If something is wrong, goes to idle 
            default:
                MAS = MonsterActionStatus.Idle;
                break;
        }
    }

    public void Hurt(float damage) {
        hp -= damage;
        if (hp <= 0) {
            DropItems((int)dropCount.Report()); // Number of dropped items
            if (dropMoney > 0) {
                GameManager.ctrl.HUDMoneyUpdate(dropMoney.RandomiseMoneyDrop(dropMoneyRange));
                GameManager.ctrl.PlaySFX(0);
            }
            if (expGained > 0)
                GameManager.ctrl.GetExpUpdate(expGained);
            GameManager.ctrl.MonsterInfoUpdate(this);//怪物資訊控制
            animCtrl.SetTrigger("Die");
            isDead = true;
            Invoke("Die", 3f);
            GameManager.ctrl.PlaySFX(2);
        }
    }

    public void Die() {
        gameObject.SetActive(false);
    }

    public void Active(bool patrol = false) {
        GameManager.ctrl.targetSystem.Add(this);
        spawnPos = transform.position;
        hp = hpMax;//初始化血量
        MAS = MonsterActionStatus.Idle;
        this.patrol = patrol;
        isDead = false;
        gameObject.SetActive(true);
    }
}
