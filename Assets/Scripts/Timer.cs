﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Timer {
    public bool isDone { get { return timeNow <= 0; } }
    public float timeNow { get { return Update(); } }
    float duration, startTime, timeLeft;

    public Timer(float duration) {
        this.duration = duration;
    }

    public Timer(float duration, bool preWarm) {
        this.duration = duration;
        if (preWarm) startTime = -duration;
    }
    /// <summary>
    /// Timer starts
    /// </summary>
    public void Start() {
        startTime = Time.time;
    }

    float Update() {
        //Debug.Log(timeLeft);
        timeLeft = duration - (Time.time - startTime);
        return (timeLeft <= 0) ? 0 : timeLeft;
    }
}
