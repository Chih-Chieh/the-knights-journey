﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoSystem : MonoBehaviour {
    public static InfoSystem ctrl;
    public UISwitch uiCtrl {
        get { return GetComponent<UISwitch>(); }
    }
    public Canvas canvas {
        get { return GetComponent<Canvas>(); }
    }


    public UIToggleCtrl toggleCtrl;
    public UIRewardPanelCtrl rewardPanelCtrl;
    public Image ExpBar;
    public Text LvlText, BalanceText, GemsText, EnergyText;
    int level = 1;
    float totalExp, currentExp, maxExp = 100f;
    int balance, gems, energy, energyMax = 100;
    float timer, recoveryTime = 3f;

    void Awake() {
        ctrl = this;
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
        
	}

    public void Init() {

        SetExp(GameManager.ctrl.gameData.totalExp);
        SetBalance(GameManager.ctrl.gameData.balance);
        SetGems(GameManager.ctrl.gameData.gems);
        SetEnergy(GameManager.ctrl.gameData.energy);
        //SetEnergy(PlayerPrefs.GetInt("Energy", energyMax));
    }

    // Update is called once per frame
    void Update () {
        if (Camera.main && canvas.worldCamera == null )
            SetupCamera();

        RecoverEnergy();

        if (Input.GetKeyDown(KeyCode.Keypad1)) {
            SetExp(100f);
        }
        if (Input.GetKeyDown(KeyCode.Keypad2)) {
            SetExp(-100f);
        }
        if (Input.GetKeyDown(KeyCode.Keypad4)) {
            SetBalance(10000);
        }
        if (Input.GetKey(KeyCode.Keypad5)) {
            SetBalance(-10000);
        }
        if (Input.GetKeyDown(KeyCode.Keypad7)) {
            SetEnergy(1);
        }
        if (Input.GetKey(KeyCode.Keypad8)) {
            SetEnergy(-1);
        }
    }
    
    public void SetupCamera () {
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = Camera.main;
        toggleCtrl.CameraSwitch();
    }

    void RecoverEnergy () {
        if (energy < energyMax) {
            timer += Time.deltaTime;
            if (timer >= recoveryTime) {
                SetEnergy(1);
                timer = 0;
                GameData.ctrl.Save();
            }
        }
    }

    public bool SetEnergy(int amount) {
        if ((energy + amount) < 0 || (amount > 0 && energy >= energyMax))
            return false;
        energy += amount;
        if (energy >= energyMax) {
            energy = energyMax;
            timer = 0; // Reset timer when energy is maxed
        }

        EnergyText.text = energy + "/" + energyMax;
        GameManager.ctrl.gameData.energy = energy; // saves to GameData
        GameData.ctrl.Save();
        return true;
    }

    public bool SetGems(int amount) {
        if ((gems + amount) < 0)
            return false;

        gems += amount;
        GemsText.text = gems.ToString();
        GameManager.ctrl.gameData.gems = gems;
        GameData.ctrl.Save();
        return true;
    }

    public bool SetBalance(int amount) {
        if ((balance + amount) < 0)
            return false;

        balance += amount;
        BalanceText.text = balance.ToString();
        GameManager.ctrl.gameData.balance = balance;
        GameData.ctrl.Save();
        return true;
    }

    public void SetExp(float gainedExp) {
        if (level > 100)
            return;
        totalExp += gainedExp; // All the exp gained
        // Forumla: (currentGainedExp - totalPrevMaxExp) / currentMaxExp
        currentExp = totalExp - TotalPreviousMaxExp(); // Actual exp after levelling up

        while (currentExp >= maxExp && level < 100) {
            LevelUp();
            currentExp = totalExp - TotalPreviousMaxExp();
        }

        ExpBar.fillAmount = currentExp / maxExp;
        GameManager.ctrl.gameData.totalExp = totalExp;
        GameData.ctrl.Save();
    }

    void LevelUp() {
        level++;
        LvlText.text = level.ToString();
        maxExp = level * 100f;
    }

    float TotalPreviousMaxExp() {
        float total = 0;
        for (int i = 1; i < level; i++) {
            total += i * 100f;
        }
        return total;
    }
}
