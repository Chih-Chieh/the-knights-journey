﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class HUDSystem : MonoBehaviour {
    public static HUDSystem ctrl;
    public Image hpBar;
    public Text hpText, moneyText, timerText;
    public UISwitch uiSwitch, pauseMenu, gameoverMenu, gamewinMenu;
    public bool isPaused, isOver;
    public Transform missionPanel;
    public UIMissionCtrl missionCtrlTMP;
    public List<UIMissionCtrl> missionCtrls = new List<UIMissionCtrl>();
    [Header("任務完成事件設定")]
    public UnityEvent events;
    [Header("關卡背景音樂編號")]
    public int bgmNumber;

    void Awake() {
        ctrl = this;
    }

	// Use this for initialization
	void Start () {
        MoneyUpdate(0);
        GameManager.ctrl.StageStart(); // set a timelimit for stage
        GameManager.ctrl.PlayBGM(bgmNumber, 0.3f);
    }

    // Update is called once per frame
    void Update() {
        if (isOver)
            return;
        if (!GameManager.ctrl.stageTimer.timeIsUp) { // if game is not over
            timerText.text = GameManager.ctrl.stageTimer.Tick(Time.deltaTime);
        } else {
            GameOver();
        }
    }

    public bool CheckMission() {
        bool isComplete = true;
        for (int i = 0; i < missionCtrls.Count; i++) {
            if (!missionCtrls[i].mission.isComplete) {
                isComplete = false;
                break;
            }
        }
        if (isComplete)
            events.Invoke();

        return isComplete;
    }

    public void GameOver() {
        isOver = true;
        uiSwitch.Switch(isOver);
        pauseMenu.Switch(false);
        gameoverMenu.Switch(isOver);
        Time.timeScale = 0;//時間軸暫停(倍率0)
    }

    public void GameWin() {
        uiSwitch.Switch(true);
        gameoverMenu.Switch(false);
        pauseMenu.Switch(false);
        gamewinMenu.Switch(true);
        Time.timeScale = 0;//時間軸暫停(倍率0)
    }


    public void Pause() {
        uiSwitch.Switch(true);
        pauseMenu.Switch(true);
        gameoverMenu.Switch(false);
        Time.timeScale = 0; // pause
    }

    public void Resume() {
        uiSwitch.Switch(false);
        Time.timeScale = 1;
    }

    public void OnFail() {
        Time.timeScale = 1;//時間軸恢復正常速率(倍率1)
        GameManager.ctrl.LoseToMenu();
    }

    public void OnWin() {
        Time.timeScale = 1;//時間軸恢復正常速率(倍率1)
        GameManager.ctrl.BackToMenu();
    }

    public void HealthBarUpdate(float hp, float hpMax) {
        hpBar.fillAmount = hp / hpMax;
        if (hpBar.fillAmount <= 0)
            GameOver(); // hp zero, opens gameover
        hpText.text = hp.ToString() + " / " + hpMax.ToString();
    }

    public void MoneyUpdate(int money) {
        moneyText.text = money.ToString();
    }

    public void CreateMission(StageMission mission) {
        UIMissionCtrl tmp = Instantiate(missionCtrlTMP, missionPanel);
        tmp.SetMissionData(mission);//介面上實體
        missionCtrls.Add(tmp);//紀錄
    }

    public void CheckMissionByMonster(int ID) {
        for (int i = 0; i < missionCtrls.Count; i++) {
            missionCtrls[i].CheckMissionData(ID);
        }
    }
}
