﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BagSlotClick : MonoBehaviour, IPointerClickHandler {

    public int index;

    public void OnPointerClick(PointerEventData eventData) {
        //print("背包欄位 " + index + " 點擊");
        GameManager.ctrl.bagSystem.UseItem(index);
    }
}
