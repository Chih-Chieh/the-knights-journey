﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData {
    public static GameData ctrl;

    public float totalExp;
    public int balance;
    public int gems;
    public int energy;
    public List<SlotData> slotDataList;
    public int slotCount {
        get { return slotDataList.Count; }
    }
    public List<SkillToggleData> skillToggleDatas;
    public int skillCount {
        get { return skillToggleDatas.Count; }
    }

    public GameData() {
        ctrl = this;
        slotDataList = new List<SlotData>();
        skillToggleDatas = new List<SkillToggleData>();
        //totalExp = PlayerPrefs.GetFloat("Exp");
        //balance = PlayerPrefs.GetInt("Balance");
        //gems = PlayerPrefs.GetInt("Gems");
        //energy = PlayerPrefs.GetInt("Energy");
    }

    public void Save() {
        PlayerPrefs.SetString("GameData", JsonUtility.ToJson(this));
        //PlayerPrefs.SetFloat("Exp", totalExp);
        //PlayerPrefs.SetInt("Balance", balance);
        //PlayerPrefs.SetInt("Gems", gems);
        //PlayerPrefs.SetInt("Energy", energy);
        //Debug.Log(JsonUtility.ToJson(this));//網路儲存
    }

    public void BagSave(SlotData[] slotDatas) {
        slotDataList = new List<SlotData>(slotDatas);
        PlayerPrefs.SetString("GameData", JsonUtility.ToJson(this));
        //Debug.Log(JsonUtility.ToJson(this));
    }

    public void SkillSave(List<SkillToggleData> skillDatas) {
        skillToggleDatas = new List<SkillToggleData>(skillDatas);
        PlayerPrefs.SetString("GameData", JsonUtility.ToJson(this));
        //Debug.Log(JsonUtility.ToJson(this));
    }
}

[System.Serializable]
public class ItemData {
    public string itemName;
    public int itemID;
    public int maxCount;
    public Sprite icon;
    public Item itemBase;
}

public class Item : ScriptableObject {
    public string itemName;
    public int itemID;
}

[CreateAssetMenu(fileName = "Consumables", menuName = "Item/Consumables", order = 0)]
public class Consumables : Item {
    public int recoveryEnergy;

    public bool Use() {
        Debug.Log("恢復能量： " + recoveryEnergy);
        return GameManager.ctrl.infoSystem.SetEnergy(recoveryEnergy);
    }
}

[System.Serializable]
public struct DropChanceInfo {
    public int itemID;
    public float chance;
}

[System.Serializable]
public struct Dice {
    public float min, max;

    public void Set(float min, float max) {
        this.min = min;
        this.max = max;
    }

    public float Report() {
        return Random.Range(min, max);
    }
}